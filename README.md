# Boilerplate para producir banners de Gears of Wars 4 tipo Sizmek

## Requerimientos
1. [Ruby](https://www.ruby-lang.org/es/downloads/)
2. [Compass](http://compass-style.org/install/)
3. [Node.js](https://nodejs.org/en/)
4. [Gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)

## Instalar dependencias
`$ npm i`

## Modo Desarrollo
Para iniciar servidor en modo desarrollo y observar los cambios en archivos correr tarea `gulp serve`

## Generar zip
Para generar el zip del banner correr la tarea `gulp build`
