"use strict";

var MU = MU || {};

(function() {
  var FALLBACK_IE_VERSION = 9; // IE version to begin fallback to static
  var tl;
  var mu_wrapper, logo, copy, cta;
  var tl_vig;
  var tl_shake;
  var tl_rain;
  var tl_txtDust;
  var tl_boom;
  var rainContainer = document.getElementById("rainHolder");
  var rainNumParticles = 100; //150;   // reduced particles for FIREFOX performance
  var rainArray = [];
  var tl_spark;
  var sparkContainer = document.getElementById("sparkHolder");
  var txtDustContainer = document.getElementById("txtHolder");
  var sparkNumParticles = 50; //75;    // reduced particles for FIREFOX performance
  var txtNumParticles = 150; //300;    // reduced particles for FIREFOX performance
  var sparkArray = [];
  var txtPartArray = [];
  var smoothStuff = [];
  var smoothStuffBG = [];
  var smoothStuffCopy = [];
  var copyBG;
  var i;

  // CHANGE VALES HERE FOR TEXT DUST
  var txtDust1StartX = 45;
  var txtDust1StopX = 310;
  var txtDust2StartX = 30;
  var txtDust2StopX = 320;
  var txtDust3StartX = 75;
  var txtDust3StopX = 310;

  var _width = 1272;
  var _height = 328;


  MU.init = function() {
    // Initialize any variables here
    mu_wrapper = MU.$('#mu_wrapper');

    smoothStuff = ["#ctaBox", "#endcardLogo", "#boxRight", "#boxLeft"];
    smoothStuffBG = ["#bg1", "#bg2", "#bg3", "#m1Body", "#m1LeftArm", "#m1LeftHand", "#m1RightArm", "#m1RightHand", "#m2Legs", "#m2Body", "#m2Arm", "#m3Body", "#m3Arm", "#m3Hand", "#m4Head", "#heroLeft", "#heroRight", "#heroMain"];
    smoothStuffCopy = ["#mushroomCloud", "#copy1", "#copy2", "#copy3", "copy4", "#endcardCopy1", "#endcardCopy2", "date"];
    copyBG = document.getElementById("copyBackground");

    tl_txtDust = new TimelineMax({
      paused: true
    });
    tl_rain = new TimelineMax({
      paused: true
    });
    tl_spark = new TimelineMax({
      paused: true
    });
    tl_boom = new TimelineMax({
      paused: true
    });

    MU.rainEmitter();
    MU.sparkEmitter();
    MU.txtEmitter();

    MU.startRain();
    MU.startTxtDust();
    MU.startSpark();


    mu_wrapper.addClass('show');

    if (useFallback()) {
      injectFallback();
    } else {
      MU.initAnimation();
      MU.startAnimation();
    }
  };

  MU.initAnimation = function() {

    //set greensock values for animation

    //this will be the ease when no ease is specified in tween params
    window.TweenLite.defaultEase = Linear.easeNone;
    window.TweenLite.ticker.fps(30);
    window.TweenLite.set(mu_wrapper, {
      autoAlpha: 1
    });
    window.TweenLite.set(rainContainer, {
      transformOrigin: "50% 50%",
      rotation: 45
    });
    window.TweenLite.set("#txtGrad, #bg1", {
      autoAlpha: 1
    });
    window.TweenLite.set(smoothStuff, {
      rotation: 0.01,
      z: 0.01
    });
    window.TweenLite.set(smoothStuffBG, {
      rotation: 0.01,
      z: -0.01
    });
    window.TweenLite.set(smoothStuffCopy, {
      rotation: 0.01,
      z: 0.00
    });
    window.TweenLite.set("#bg2, #bg3", {
      autoAlpha: 0
    });
    window.TweenLite.set(copyBG, {
      autoAlpha: 0
    });
    window.TweenLite.set("#smokeBalls", {
      autoAlpha: 0
    });



    tl_boom.add(MU.mushroomCloudTL(), 0);

    tl = new TimelineMax({
      delay: 0.0,
      onComplete: MU.onAnimationComplete
    });
    tl.timeScale(1.0);
    var backEase = Back.easeOut.config(1.3);


    /////////////////////////////////////////////////////////////////
    // FRAME 1
    /////////////////////////////////////////////////////////////////
    tl.addLabel("frame1");

    tl.add(MU.rainPlay, "frame1");

    tl.add([
      TweenLite.fromTo("#m1Container", 0.2, {
        x: -45,
        y: -15
      }, {
        x: -5,
        y: 20
      }),
      TweenLite.to("#m1Container", 1.5, {
        y: 5,
        x: 5,
        ease: Power2.easeInOut
      }),
    ], "frame1", "sequence");

    tl.add(MU.shakeCam, "frame1+=0.25");
    tl.to("#bg1", 1.25, {
      scale: 1.1,
      x: -10,
      y: -5
    }, "frame1");
    tl.fromTo("#m1Body", 1.35, {
      x: -24,
      y: -7,
      rotation: -10
    }, {
      x: 0,
      y: 0,
      rotation: 0,
      ease: Power4.easeOut
    }, "frame1");

    tl.add([
      TweenLite.fromTo("#m1LeftArm", 0.2, {
        rotation: 20
      }, {
        rotation: 10
      }),
      TweenLite.to("#m1LeftArm", 0.2, {
        rotation: -10,
        ease: Power2.easeOut
      }),
      TweenLite.to("#m1LeftArm", 1.8, {
        rotation: 50
      }),
    ], "frame1", "sequence");

    tl.fromTo("#m1LeftHand", 2, {
      rotation: 40
    }, {
      rotation: -30,
      ease: Power2.easeOut
    }, "frame1+=0.2");

    tl.add([
      TweenLite.fromTo("#m1RightArm", 0.8, {
        x: -10,
        y: -10,
        rotation: -30
      }, {
        x: -5,
        y: 0,
        rotation: -5,
        ease: backEase
      }),
      TweenLite.to("#m1RightArm", 1, {
        rotation: -10,
        ease: Power1.easeIn
      }),
    ], "frame1", "sequence");

    tl.fromTo("#m1RightHand", 0.5, {
      rotation: 20
    }, {
      rotation: 30,
      ease: backEase
    }, "frame1");


    /////////////////////////////////////////////////////////////////
    // FRAME 2
    /////////////////////////////////////////////////////////////////
    tl.addLabel("frame2", "-=1");

    tl.add(MU.sparkPlay, "frame2");

    tl.set(copyBG, {
      autoAlpha: 1
    }, "frame2");
    tl.set("#bg1", {
      autoAlpha: 0
    }, "frame2");
    tl.fromTo("#copy1", 1.0, {
      autoAlpha: 0
    }, {
      autoAlpha: 1,
      ease: RoughEase.ease.config({
        template: Power0.easeNone,
        strength: 1.5,
        points: 25,
        taper: "out",
        randomize: true,
        clamp: false
      })
    }, "frame2");
    tl.fromTo("#copy1", 3.5, {
      transformOrigin: "50% 50%",
      scale: 1.1
    }, {
      scale: 0.8,
      ease: Power2.easeOut
    }, "frame2");


    // Mushroom cloud background
    tl.add(MU.mushroomCloud, "frame2-=0.75");
    tl.add(MU.shakeCam, "frame2+=0.25");


    /////////////////////////////////////////////////////////////////
    // FRAME 3
    /////////////////////////////////////////////////////////////////
    tl.addLabel("frame3", "frame2+=2.45");
    tl.add(MU.rainPlay, "frame3");

    tl.staggerTo("#copy1 .whiteFill", 0.4, {
      transformOrigin: "80% 100%",
      scaleX: 0.2,
      fill: "#000000",
      ease: Power1.easeIn
    }, 0.1, "frame3-=1.5");
    tl.fromTo(txtDustContainer, 1.4, {
      x: 380
    }, {
      x: 880,
      ease: Power1.easeIn,
      onStart: MU.txtDustPlay
    }, "frame3-=1.55");
    tl.add([
      TweenLite.fromTo(txtDustContainer, 0.4, {
        immediateRender: false,
        y: 45
      }, {
        y: 55,
        ease: Power1.easeInOut
      }),
      TweenLite.to(txtDustContainer, 0.4, {
        y: 47,
        ease: Power1.easeInOut
      }),
      TweenLite.to(txtDustContainer, 0.4, {
        y: 53,
        ease: Power1.easeInOut
      }),
    ], "frame3-=1.55", "sequence");

    tl.add([
      TweenLite.fromTo("#endStop", 0.5, {
        attr: {
          offset: "0%"
        }
      }, {
        attr: {
          offset: "100%"
        },
        ease: Power2.easeOut
      }),
      TweenLite.to("#endStop", 0.7, {
        attr: {
          offset: "0%"
        },
        ease: Power2.easeIn
      }),
    ], "frame3-=1.15", "sequence");


    tl.set(txtDustContainer, {
      autoAlpha: 0
    }, "frame3");


    tl.set("#bg2", {
      autoAlpha: 1
    }, "frame3-=0.5");
    tl.set(copyBG, {
      autoAlpha: 0
    }, "frame3");


    tl.add(MU.shakeCam, "frame3+=0.25");

    tl.to("#bg2", 1.9, {
      scale: 1.15,
      x: 16,
      y: 10
    }, {
      x: 0,
      y: 0,
      scale: 1.001,
      ease: Power1.easeOut
    }, "frame3");
    tl.from("#m2Legs", 1.25, {
      x: 15,
      y: 1,
      rotation: 6,
      ease: Power1.easeInOut
    }, "frame3");
    tl.from("#m2Body", 1.25, {
      x: 0,
      y: 0,
      rotation: 10,
      ease: Power1.easeOut
    }, "frame3");
    tl.from("#m2Arm", 1.25, {
      x: 0,
      y: 0,
      rotation: -30,
      ease: Power1.easeInOut
    }, "frame3");
    tl.from("#m3BodyContainer", 1.5, {
      x: 30,
      y: -10,
      rotation: 10,
      ease: Power2.easeOut
    }, "frame3+=0.0");
    tl.from("#m3ArmContainer", 1.25, {
      x: 0,
      y: -10,
      rotation: 30,
      ease: Power2.easeInOut
    }, "frame3+=0.1");
    tl.to("#m3Hand", 1.5, {
      x: -6,
      y: 2,
      rotation: -35,
      ease: Power2.easeInOut
    }, "frame3+=0.4");
    tl.from("#m4Head", 1.40, {
      x: 90,
      y: -70,
      rotation: 15,
      ease: Power1.easeInOut
    }, "frame3");
    tl.from("#m4Hand", 1.40, {
      x: 130,
      y: -50,
      rotation: -35,
      ease: Power1.easeInOut
    }, "frame3");


    /////////////////////////////////////////////////////////////////
    // FRAME 4
    /////////////////////////////////////////////////////////////////
    tl.addLabel("frame4", "frame3+=1");

    tl.add(MU.sparkPlay, "frame4");

    // Mushroom cloud background
    tl.add(MU.mushroomCloud, "frame4-=0.75");
    tl.add(MU.shakeCam, "frame4+=0.25");

    tl.set(copyBG, {
      autoAlpha: 1
    }, "frame4");
    tl.set("#bg2", {
      autoAlpha: 0
    }, "frame4+=0.3");
    tl.fromTo("#copy2", 1.0, {
      autoAlpha: 0
    }, {
      autoAlpha: 1,
      ease: RoughEase.ease.config({
        template: Power0.easeNone,
        strength: 1.5,
        points: 25,
        taper: "out",
        randomize: true,
        clamp: false
      })
    }, "frame4");
    tl.fromTo("#copy2", 3.5, {
      transformOrigin: "50% 50%",
      scale: 1.1
    }, {
      scale: 0.8,
      ease: Power2.easeOut
    }, "frame4");


    /////////////////////////////////////////////////////////////////
    // FRAME 5
    /////////////////////////////////////////////////////////////////
    tl.addLabel("frame5", "-=1.35");
    tl.add(MU.rainPlay, "frame5");

    tl.staggerTo(".c2l1", 0.4, {
      transformOrigin: "80% 100%",
      scaleX: 0.2,
      fill: "#000000",
      ease: Power1.easeIn
    }, 0.1, "frame5-=1");
    tl.staggerTo(".c2l2", 0.4, {
      transformOrigin: "80% 100%",
      scaleX: 0.2,
      fill: "#000000",
      ease: Power1.easeIn
    }, 0.08, "frame5-=1.2");
    tl.fromTo(txtDustContainer, 1.4, {
      immediateRender: false,
      autoAlpha: 1,
      x: 420
    }, {
      x: 1020,
      ease: Power1.easeIn
    }, "frame5-=1.2");
    tl.add(MU.txtDustPlay, "frame5-=1.2");
    tl.add([
      TweenLite.fromTo(txtDustContainer, 0.7, {
        immediateRender: false,
        y: 40
      }, {
        y: 50,
        ease: Power1.easeInOut
      }),
      TweenLite.to(txtDustContainer, 0.6, {
        y: 55,
        ease: Power1.easeInOut
      }),
    ], "frame5-=1.2", "sequence");
    tl.add([
      TweenLite.to("#endStop", 0.5, {
        attr: {
          offset: "100%"
        },
        ease: Power2.easeOut
      }),
      TweenLite.to("#endStop", 0.7, {
        attr: {
          offset: "0%"
        },
        ease: Power2.easeIn
      }),
    ], "frame5-=1.2", "sequence");

    tl.set(txtDustContainer, {
      autoAlpha: 0
    }, "frame5");


    tl.set("#bg3", {
      autoAlpha: 1
    }, "frame5-=0.5");
    tl.set(copyBG, {
      autoAlpha: 0
    }, "frame5");

    tl.add(MU.shakeCam, "frame5");
    tl.fromTo("#bg3", 2.5, {
      x: 30,
      y: -15,
      scale: 1.4
    }, {
      x: 0,
      y: -5,
      scale: 1.1
    }, "frame5");
    tl.from("#heroLeft", 2, {
      x: 57,
      y: 30,
      rotation: -5,
      ease: Power2.easeOut
    }, "frame5");

    tl.from("#heroMain", 2, {
      x: 15,
      y: 40,
      rotation: 5,
      ease: Power2.easeOut
    }, "frame5");

    tl.add([
      TweenLite.fromTo("#lightning", 0.4, {
        immediateRender: false,
        autoAlpha: 0.9
      }, {
        autoAlpha: 0.1,
        ease: Power4.easeIn
      }),
      TweenLite.to("#lightning", 0.1, {
        autoAlpha: 0.1
      }),
      TweenLite.to("#lightning", 0.1, {
        autoAlpha: 0.3
      }),
      TweenLite.to("#lightning", 0.05, {
        autoAlpha: 0.1
      }),
      TweenLite.to("#lightning", 0.05, {
        autoAlpha: 0.3
      }),
      TweenLite.to("#lightning", 0.6, {
        autoAlpha: 0
      }),
    ], "frame5+=0.2", "sequence");


    /////////////////////////////////////////////////////////////////
    // FRAME 6
    /////////////////////////////////////////////////////////////////
    tl.addLabel("frame6", "frame5+=1.4");

    tl.add(MU.sparkPlay, "frame6");

    tl.set(copyBG, {
      autoAlpha: 1
    }, "frame6");
    tl.fromTo("#copy3", 1.0, {
      autoAlpha: 0
    }, {
      autoAlpha: 1,
      ease: RoughEase.ease.config({
        template: Power0.easeNone,
        strength: 1.5,
        points: 25,
        taper: "out",
        randomize: true,
        clamp: false
      })
    }, "frame6");
    tl.fromTo("#copy3", 3.5, {
      transformOrigin: "50% 50%",
      scale: 1.1
    }, {
      scale: 0.8,
      ease: Power2.easeOut
    }, "frame6");

    // Mushroom cloud background
    tl.add(MU.mushroomCloud, "frame6-=0.75");
    tl.add(MU.shakeCam, "frame6+=0.25");


    /////////////////////////////////////////////////////////////////
    // FRAME 7
    /////////////////////////////////////////////////////////////////
    tl.addLabel("frame7", "-=1.35");
    tl.add(MU.rainPlay, "frame7");

    tl.staggerTo("#copy3 .whiteFill", 0.4, {
      transformOrigin: "80% 100%",
      scaleX: 0.2,
      fill: "#000000",
      ease: Power1.easeIn
    }, 0.1, "frame7-=1");
    tl.fromTo(txtDustContainer, 1.4, {
      immediateRender: false,
      autoAlpha: 1,
      x: 475
    }, {
      x: 960,
      ease: Power1.easeIn,
      onStart: MU.txtDustPlay
    }, "frame7-=1.15");
    tl.add([
      TweenLite.fromTo(txtDustContainer, 0.4, {
        immediateRender: false,
        y: 45
      }, {
        y: 50,
        ease: Power1.easeInOut
      }),
      TweenLite.to(txtDustContainer, 0.4, {
        y: 48,
        ease: Power1.easeInOut
      }),
      TweenLite.to(txtDustContainer, 0.4, {
        y: 53,
        ease: Power1.easeInOut
      }),
    ], "frame7-=1.15", "sequence");
    tl.add([
      TweenLite.to("#endStop", 0.5, {
        attr: {
          offset: "100%"
        },
        ease: Power2.easeOut
      }),
      TweenLite.to("#endStop", 0.7, {
        attr: {
          offset: "0%"
        },
        ease: Power2.easeIn
      }),
    ], "frame7-=1.15", "sequence");

    tl.fromTo("#copy4", 1.0, {
      autoAlpha: 0
    }, {
      autoAlpha: 1,
      ease: RoughEase.ease.config({
        template: Power0.easeNone,
        strength: 1.5,
        points: 25,
        taper: "out",
        randomize: true,
        clamp: false
      })
    }, "frame7");
    tl.fromTo("#copy4", 2, {
      transformOrigin: "50% 50%",
      scale: 1.3
    }, {
      scale: 1,
      ease: Power2.easeOut
    }, "frame7");

    tl.from("#vignette2", 0.5, {
      autoAlpha: 0
    }, "frame7");
    tl.set(txtDustContainer, {
      autoAlpha: 0
    }, "frame7");
    tl.set(copyBG, {
      autoAlpha: 0
    }, "frame7");

    tl.set("#heroesContainer", {
      scale: 1,
      x: 370,
      y: 0
    }, "frame7"); /*scale: 0.78*/
    tl.fromTo("#bg3", 2, {
      immediateRender: false,
      scale: 1.2
    }, {
      x: 0,
      y: 0,
      scale: 1,
      ease: Power2.easeOut
    }, "frame7");

    tl.fromTo("#heroRight", 0.4, {
      autoAlpha: 0
    }, {
      autoAlpha: 1
    }, "frame7-=0.3");
    tl.from("#heroRight", 2, {
      x: -27,
      y: 20,
      rotation: -5,
      ease: Power2.easeOut
    }, "frame7");

    // tl.add([
    //   TweenLite.fromTo("#lightning", 0.4, {
    //     immediateRender: false,
    //     autoAlpha: 0.9
    //   }, {
    //     autoAlpha: 0.1,
    //     ease: Power4.easeIn
    //   }),
    //   TweenLite.to("#lightning", 0.1, {
    //     autoAlpha: 0.1
    //   }),
    //   TweenLite.to("#lightning", 0.1, {
    //     autoAlpha: 0.3
    //   }),
    //   TweenLite.to("#lightning", 0.05, {
    //     autoAlpha: 0.1
    //   }),
    //   TweenLite.to("#lightning", 0.05, {
    //     autoAlpha: 0.3
    //   }),
    //   TweenLite.to("#lightning", 0.6, {
    //     autoAlpha: 0
    //   }),
    // ], "frame7+=0.4", "sequence");

    tl_vig = new TimelineMax({
      repeat: -1
    });

    tl_vig.add([
      TweenLite.fromTo("#vignette", 0.1, {
        autoAlpha: 0.35
      }, {
        autoAlpha: 0.2
      }),
      TweenLite.to("#vignette", 0.1, {
        autoAlpha: 0.45
      }),
      TweenLite.to("#vignette", 0.07, {
        autoAlpha: 0.1
      }),
    ], 0, "sequence");

    // tl.fromTo("#knowMore", 0.4, {
    //   autoAlpha: 0,
    //   scale: 0.7
    // }, {
    //   autoAlpha: 0.85,
    //   scale: 1,
    //   ease: Power4.easeIn
    // }, "frame7+=1.4");
    /////////////////////////////////////////////////////////////////
    // FRAME 8
    /////////////////////////////////////////////////////////////////
    tl.addLabel("endcard", "+=0.6")

    tl.to('#copy4', 0.25, {
      autoAlpha:0
    }, 'endcard')
    tl.to("#heroMain", 0.5, {
      autoAlpha: 0,
      scale: 1.3,
      ease: Power1.easeOut
    }, "endcard")

    tl.fromTo("#endcardLogo", 1.55, {
      autoAlpha: 0
    }, {
      autoAlpha: 1,
      y: 0,
      ease: Power1.easeOut
    }, "endcard")

    tl.to("#heroLeft", 0.5, {
      autoAlpha: 0,
      x: -60,
      ease: Power1.easeOut
    }, "endcard")

    tl.to("#heroRight", 0.5, {
      autoAlpha: 0,
      x: 60,
      ease: Power1.easeOut
    }, "endcard")

    var dur1 = 0.15;
    var dur2 = 2.1;

    tl.add([
      TweenLite.from("#boxContainer", 0.2, {
        transformOrigin: "50% 30%",
        scale: 0,
        x: 0
      }, {
        scale: 0.75,
        x: 0,
        ease: Power4.easeIn
      }),
      TweenLite.to("#boxContainer", 2, {
        x: 0,
        scale: 1,
        autoAlpha: 1,
        ease: Power2.easeOut
      }),
    ], "endcard+=0.3", "sequence");

    tl.add([
      TweenLite.fromTo("#boxRight", dur1, {
        scaleX: 1,
        y : 5
      }, {
        scaleX: .9
      }),
      TweenLite.to("#boxRight", dur2, {
        scaleX: 1,
        ease: Power2.easeOut
      }),
    ], "endcard+=0.3", "sequence");

    tl.fromTo("#knowMore", 0.3, {
      autoAlpha: 0
    }, {
      autoAlpha: 0.85,
      ease: Power1.easeOut
    }, "endcard+=0.8")

    tl.add([
      TweenLite.fromTo("#knowMore", dur1, {
        x: 170
      }, {
        x: 10
      }),
      TweenLite.to("#knowMore", dur2, {
        x: 0,
        ease: Power2.easeOut
      }),
    ], "endcard+=0.6", "sequence");
  };

  /////////////////////////////////////////////////////////////////
  // ANIMATION FUNCTIONS
  /////////////////////////////////////////////////////////////////

  MU.startAnimation = function() {
    // Code for animation
    tl.play();
  };

  MU.onTick = function() {
    // Interval for updating on requestAnimationFrame
  };

  MU.onAnimationComplete = function() {
    // Log duration of tl
    console.log('Animation Duration: ' + tl.time() + 's');
  };

  MU.ctaOverHandler = function(e) {
    console.log("CTAover");

    TweenLite.to("#ctaBox", 0.15, {
      backgroundColor: "#a31c11;"
    });
  };

  MU.ctaOutHandler = function(e) {
    console.log("CTAout");
    TweenLite.to("#ctaBox", 0.15, {
      backgroundColor: "#e02718;"
    });

  };

  MU.ctaClickHandler = function(e) {
    console.log("Click");
  };

  MU.txtDustPlay = function() {
    tl_txtDust.play(0);
  }
  MU.rainPlay = function() {
    tl_rain.play();
    tl_spark.pause();
  }
  MU.sparkPlay = function() {
    tl_spark.play();
    tl_rain.pause();
  }
  MU.mushroomCloud = function() {
    console.log("play mush");
    tl_boom.play(0);
  }
  MU.mushroomCloudTL = function() {
    console.log("Boom");
    var tl = new TimelineMax();
    tl.set("#mushroomCloud", {
      autoAlpha: 0.9
    }, 0);
    tl.fromTo("#smokeBalls", 0.3, {
      autoAlpha: 0
    }, {
      autoAlpha: 1
    }, 0.85);

    tl.fromTo("#mushroomCloud", 2.5, {
      transformOrigin: "50% 100%",
      scale: 1
    }, {
      scale: 1.04,
      ease: Power0.easeNone
    }, 0.75);

    tl.add([
      TweenLite.fromTo("#ball1", 4.2, {
        transformOrigin: "50% 50%",
        x: 50,
        y: 175
      }, {
        y: -5,
        ease: Power2.easeOut
      }),
      TweenLite.fromTo("#ball1", 4.0, {
        scale: 0.5
      }, {
        scale: 1.2,
        ease: Power1.easeOut
      }),
    ], 0, "normal");

    tl.add([
      TweenLite.fromTo("#ball2", 4.75, {
        transformOrigin: "50% 50%",
        y: 150
      }, {
        y: 12,
        ease: Power2.easeOut
      }),
      TweenLite.fromTo("#ball2", 0.75, {
        x: 175
      }, {
        x: 125,
        ease: Power1.easeOut
      }),
      TweenLite.to("#ball2", 2.75, {
        x: 150,
        delay: 0.65,
        ease: Power2.easeOut
      }),
    ], 0.75, "normal");

    tl.add([
      TweenLite.fromTo("#ball3", 4.75, {
        transformOrigin: "50% 50%",
        y: 150,
        scale: 1
      }, {
        y: 10,
        scale: 1.1,
        ease: Power2.easeOut
      }),
      TweenLite.fromTo("#ball3", 1.0, {
        x: 20
      }, {
        x: 80,
        ease: Power1.easeOut
      }),
      TweenLite.to("#ball3", 2.5, {
        x: 10,
        delay: 0.9,
        ease: Power1.easeOut
      }),
    ], 0.5, "normal");

    return tl;
  }

  MU.shakeCam = function() {
    console.log("Shake");

    tl_shake = new TimelineMax();

    tl_shake.add([
      TweenLite.fromTo("#sceneContainer", 0.10, {
        x: 0,
        y: 0
      }, {
        x: Math.random() * 8 - 4,
        y: Math.random() * 6 - 3
      }),
      TweenLite.to("#sceneContainer", 0.10, {
        x: Math.random() * 8 - 4,
        y: Math.random() * 6 - 3
      }),
      TweenLite.to("#sceneContainer", 0.10, {
        x: Math.random() * 8 - 4,
        y: Math.random() * 6 - 3
      }),
      TweenLite.to("#sceneContainer", 0.10, {
        x: Math.random() * 8 - 4,
        y: Math.random() * 6 - 3
      }),
      TweenLite.to("#sceneContainer", 0.10, {
        x: Math.random() * 8 - 4,
        y: Math.random() * 6 - 3
      }),
      TweenLite.to("#sceneContainer", 0.10, {
        x: 0,
        y: 0
      }),
    ], 0, "sequence");
  }

  MU.startRain = function() {
    for (i = 0; i < rainNumParticles; i++) {
      tl_rain.add(MU.rainAnimation(rainArray), i * 0.01);
    }
  }
  MU.rainAnimation = function(dA) {
    console.log("emitting rain!!");
    var randDur = Math.random() * 0.7 + 0.2;
    var tl_rainy = new TimelineMax({
      repeat: -1
    });
    //random alpha
    tl_rainy.set(dA[i], {
      autoAlpha: Math.random() * 0.1 + 0.2
    }, 0);
    //random start scale
    tl_rainy.set(dA[i], {
      scaleY: Math.random() * 0.4 + 0.3
    }, 0);

    //random x starting pos and random duration.
    tl_rainy.fromTo(dA[i], randDur, {
      x: Math.random() * (_width + 100),
      y: -10
    }, {
      y: (_height + 150)
    }, 0);

    //scale
    tl_rainy.add([
      TweenLite.to(dA[i], randDur / 2, {
        scaleY: Math.random() * 1 + 4,
        ease: Power3.easeIn
      }),
      TweenLite.to(dA[i], randDur / 2, {
        scaleY: Math.random() * 0.4 + 0.2,
        ease: Power2.easeInOut
      }),
    ], 0, "sequence");

    return tl_rainy;
  };

  MU.rainEmitter = function() {
    console.log("creating rain!!");

    for (i = 0; i < rainNumParticles; i++) {
      var rain = document.createElement('div');
      rain.className = "rainClass";
      rainContainer.appendChild(rain);

      rainArray.push(rain);
    }
  }

  MU.sparkAnimation = function(dA) {
    console.log("emitting sparks!!");
    var randDur = Math.random() * 3 + 2.0;
    var tl_sparky = new TimelineMax({
      repeat: -1
    });
    //random alpha
    tl_sparky.set(dA[i], {
      autoAlpha: Math.random() * 0.4 + 0.2
    }, 0);
    //random start scale
    tl_sparky.set(dA[i], {
      scaleY: Math.random() * 0.4 + 0.3
    }, 0);

    //random x starting pos and random duration.
    tl_sparky.fromTo(dA[i], randDur, {
      x: Math.random() * (_width + 100),
      y: (_height + 50)
    }, {
      bezier: {
        values: [{
          x: Math.random() * (_width + 100),
          y: -25
        }, {
          x: Math.random() * (_width + 100),
          y: -100
        }, {
          x: Math.random() * (_width + 100),
          y: -250
        }],
        autoRotate: true
      }
    }, 0);

    //scale
    //        tl_sparky.to(dA[i], randDur/2, {scale: Math.random()*0+0.1, ease: Power3.easeIn}, 0);
    tl_sparky.add([
      TweenLite.to(dA[i], randDur / 2, {
        scaleX: Math.random() * 0.25 + 1,
        ease: Power3.easeIn
      }),
      TweenLite.to(dA[i], randDur / 2, {
        scaleX: Math.random() * 1.5 + 3.0,
        ease: Power2.easeInOut
      }),
    ], 0, "sequence");

    return tl_sparky;
  };

  MU.startSpark = function() {
    for (i = 0; i < sparkNumParticles; i++) {
      tl_spark.add(MU.sparkAnimation(sparkArray), i * 0.01);
    }
  }

  MU.sparkEmitter = function() {
    console.log("creating sparks!!");

    for (i = 0; i < sparkNumParticles; i++) {
      var spark = document.createElement('div');
      if (i % 2) {
        spark.className = "sparkClass2";
      } else {
        spark.className = "sparkClass";
      }

      sparkContainer.appendChild(spark);

      sparkArray.push(spark);
    }
  }

  MU.startTxtDust = function() {
    for (i = 0; i < txtNumParticles; i++) {
      //tl_txtDust.add(MU.txtAnimation(txtPartArray), i*0.003);
      tl_txtDust.add(MU.txtAnimation(txtPartArray), i * 0.007); // reduced to 150 particles for FIREFOX performance
    }
  }

  MU.txtAnimation = function(tPA) {
    console.log("emitting txt dust!!");

    var tl = new TimelineMax();
    var randDur = Math.random() * 0.2 + 0.8;

    tl.to(tPA[i], 0.001, {
      autoAlpha: Math.random() * 0.6 + 0.4
    }, "start");
    tl.fromTo(tPA[i], 1, {
      rotation: Math.random() * 360
    }, {
      rotation: Math.random() * 360,
      ease: Power3.easeOut
    }, "start");
    tl.fromTo(tPA[i], 1, {
      scaleX: Math.random() * 2 + 0.8,
      scaleY: Math.random() * 2 + 0.8
    }, {
      scaleX: Math.random() * 0.8 + 0.2,
      scaleY: Math.random() * 0.8 + 0.2
    }, "start");
    tl.fromTo(tPA[i], 1, {
      y: Math.random() * 20 - 10
    }, {
      x: Math.random() * (i * 4) + 125,
      y: Math.random() * 140 - (70 + i * 0.7)
    }, "start");
    tl.to(tPA[i], 0.9, {
      autoAlpha: 0
    }, "start+=0.1");




    return tl;

  };

  MU.txtEmitter = function() {
    console.log("creating txt dust!!");

    for (i = 0; i < txtNumParticles; i++) {
      var txtPart = document.createElement('div');

      if (i % 3) {
        txtPart.className = "txtDustClass2";
      } else {
        txtPart.className = "txtDustClass";
      }
      txtDustContainer.appendChild(txtPart);

      txtPartArray.push(txtPart);
    }
  }

  MU.killEffects = function() {
    tl_vig.clear();
    tl_rain.clear();
    tl_spark.clear();
    tl_txtDust.clear();
  }

  // animators can pretty much ignore anything below
  // Sniff user agent for IE version and show fallback if it fails
  function useFallback() {
    var rv = -1;
    var ua = navigator.userAgent;
    var re = new RegExp('MSIE ([0-9]{1,}[\.0-9]{0,})');

    if (re.exec(ua) != null)
      rv = parseFloat(RegExp.$1);


    if (rv > 0 && rv <= FALLBACK_IE_VERSION)
      return true;
    else
      return false;
  }

  function injectFallback() {
    var body = document.body;

    while (body.firstChild) {
      body.removeChild(body.firstChild);
    }

    var anchor = document.createElement('a');
    anchor.href = window.clickVariable;
    anchor.target = '_blank';

    var img = new Image();
    img.src = 'img/static.jpg';

    anchor.appendChild(img);
    document.body.appendChild(anchor);
  };


  /**
   * Jquery-like selector and helper methods
   * Selector: MU.$( 'el' )
   * HasClass: MU.$( '#id' ).hasClass( 'class' );
   * AddClass: MU.$( '.class' ).addClass( 'class' )
   * RemoveClass: MU.$( '#id .class' ).removeClass( 'class' )
   **/
  MU.$ = function(el) {
    var supportsQuerySelectorAll = !!document.querySelectorAll,
      elObj;

    if (supportsQuerySelectorAll) {
      try {
        elObj = document.querySelectorAll(el);

        elObj.hasClass = hasClass;
        elObj.addClass = addClass;
        elObj.removeClass = removeClass;
      } catch (e) {}
    }

    return elObj;
  }

  function hasClass(className) {
    if (typeof className === 'string') {
      for (var i = 0; i < this.length; i++) {
        if (this[i].nodeType === 1 && this[i].className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))) {
          return true;
        }
      }
    }

    return false;
  }

  function addClass(className) {
    if (typeof className === 'string') {
      for (var i = 0; i < this.length; i++) {
        if (!this.hasClass(className)) {
          for (var j = 0; j < this.length; j++) {
            this[j].className += (this[j].className == "" ? "" : " ") + className;
          }
        }
      }
    }

    return this;
  }

  function removeClass(className) {
    if (typeof className === 'string') {
      for (var i = 0; i < this.length; i++) {
        if (this.hasClass(className)) {
          var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
          this[i].className = this[i].className.replace(reg, ' ');
        }
      }
    }

    return this;
  }
})();
