/*******************
 VARIABLES
*******************/
	var creativeVersion = "1.0.0";	// format versioning code, please do not alter or remove this variable
	var customJSVersion = null;		// format versioning code, please do not alter or remove this variable 
	var closeButton;
	var userActionButton;
	var clickthroughButton;
	var isAndroid2 = (/android 2/i).test(navigator.userAgent);
	var android2ResizeTimeout;
	
	window.addEventListener("load", checkIfEBInitialized);
	window.addEventListener("message", onMessageReceived);
	window.addEventListener("resize",setColumn);
	
/*******************
 INITIALIZATION
*******************/
	function checkIfEBInitialized(event) 
	{
		if (EB.isInitialized()) initializeCreative();
		else EB.addEventListener(EBG.EventName.EB_INITIALIZED, initializeCreative);
		setColumn()
		setTimeout(function(){ setColumn();}, 500);
	}
	
	function initializeCreative(event) 
	{
		initCustomVars();
		initializeGlobalVariables();
		addEventListeners();
		initExpandAnimation();		
		setCreativeVersion();		// format versioning code, please do not alter or remove this function
		closeButton.style.display="none";
		setColumn()
		setTimeout(function(){ setColumn();}, 500);
	}

	function initExpandAnimation(){
		var params 	= params || {};
		params.eventName 			= 'INIT_PANEL_EXPANSION';
		params.panelName 			= mdCustomHeaderPanelName;
		EB._sendMessage('dispatchCustomScriptEvent', params);
		MU.init();
	}

	function initializeGlobalVariables() 
	{
		closeButton = document.getElementById("close-button");
		userActionButton = document.getElementById("user-action-button");
		clickthroughButton = document.getElementById("clickthrough-button");
	}
	
	function addEventListeners() 
	{
		userActionButton.addEventListener("click", userAction);
		clickthroughButton.addEventListener("click", clickthrough);
		closeButton.addEventListener("click", collapse);
	}

	function removeEventListeners() 
	{
		userActionButton.removeEventListener("click", userAction);
		clickthroughButton.removeEventListener("click", clickthrough);
		closeButton.removeEventListener("click", collapse);
	}

	function initCustomVars() 
	{
		setCustomVar("mdCustomHeaderPanelName", 'CustomHeader');
		setCustomVar("mdCloseButtonShow", false);
	}
	
	function checkCloseEnabled()
	{
		if(!mdCloseButtonShow){
			closeButton.style.display="none";
		} else {closeButton.style.display="block";}
	}
/*******************
 EVENT HANDLERS
*******************/
	function userAction(event) 
	{
		EB.userActionCounter("UserAction");
	}
	
	function clickthrough(event) 
	{
		EB.clickthrough();
	}
	
	function expand(event) 
	{
		EB.expand();
	}
		
	function collapse(event) 
	{
		var params 	= params || {};
		params.eventName 			= 'COLLAPSE_CUSTOM_HEADER';
		params.panelName 			= mdCustomHeaderPanelName;
		EB._sendMessage('dispatchCustomScriptEvent', params);
	}

	function collapseCompletely(){
		EB.collapse({
			panelName: mdCustomHeaderPanelName,
			actionType: EBG.ActionType.USER
		});
	}

	function onMessageReceived(event) 
	{
		EBG.log.debug("event: "+event);
		try {
			var messageData = JSON.parse(event.data);
		
			if (messageData.adId && messageData.adId === getAdID()) 
			{
				if (messageData.type && messageData.type === "resize") 
				{
					if (isAndroid2) android2OnResizeHandler();
				}else if (messageData.type && messageData.type === "EXPAND_ANIM_START") 
				{
					
				}else if (messageData.type && messageData.type === "COLLAPSE_ANIM_START") 
				{
					closeButton.style.display="none";
				}else if (messageData.type && messageData.type === "EXPAND_ANIM_COMPLETE") 
				{
					checkCloseEnabled()	
				}else if (messageData.type && messageData.type === "COLLAPSE_ANIM_COMPLETE") 
				{
					collapseCompletely();
				}
			}
		} 
		catch (error) {
			EBG.log.debug(error);
		}
	}

/*******************
 UTILITIES
*******************/
	function getAdID() 
	{
		if (EB._isLocalMode) return null;
		else return EB._adConfig.adId;
	}

	function setCustomVar(customVarName, defaultValue, parseNum) {	//create global var with name = str, taking value from adConfig if it's there, else use default
		var value = defaultValue;
		if(!EB._isLocalMode){
			var value = EB._adConfig.hasOwnProperty(customVarName) ? EB._adConfig[customVarName] : defaultValue;
		}
		if (arguments.length == 3 && parseNum && typeof value === "string") value = parseFloat(value);
		window[customVarName] = value;
	}
	
	function android2OnResizeHandler() 
	{
		document.body.style.opacity = 0.99
		clearTimeout(android2ResizeTimeout);
		
		android2ResizeTimeout = setTimeout(function() {
			document.body.style.opacity = 1;
			document.body.style.height = window.innerHeight;
			document.body.style.width = window.innerWidth;
		}, 200);
	}
	

	function setColumn()
	{
		var columnWidth = document.getElementById("mu_wrapper").offsetWidth;
		
		if(columnWidth < 819)
		{
			EB.automaticEventCounter('Two_Columns_Viewed');
			setCurrentColumn(1);
		}else if(columnWidth < 949 && columnWidth >= 819)
		{
			EB.automaticEventCounter('Three_Columns(Scale_Down)_Viewed');
			setCurrentColumn(2);
		}else if(columnWidth < 1118 && columnWidth >= 949)
		{
			EB.automaticEventCounter('Three_Columns_Viewed');
			setCurrentColumn(3);
		}else if(columnWidth < 1271 && columnWidth >= 1118)
		{
			EB.automaticEventCounter('Four_Columns(Scale_Down)_Viewed');
			setCurrentColumn(4);
		}else if(columnWidth >= 1271)
		{
			EB.automaticEventCounter('Four_Columns_Viewed');
			setCurrentColumn(5);
		}
	}

	function setCurrentColumn(n)
	{
		for (var i = 1; i < 6; i++) {
			document.getElementById(("column")+i).style.display="none";
		};
		document.getElementById(("column")+n).style.display="block";
	}
	/* versioning display function starts, you may remove these functions from your product */

	function displayVersion(version)
	{
		var divTag = document.createElement("div");
		divTag.className = version.className;
		divTag.innerHTML = version.label + ": " + version.version;
		document.getElementsByTagName("body")[0].appendChild(divTag);
	}

	function displayCreativeVersion()
	{
		displayVersion({
			label: "Creative Version",
			version: creativeVersion,
			className: "creativeVersion"
		});
	}

	function displayCustomJSVersion()
	{
		displayVersion({
			label: "Custom JS Version",
			version: customJSVersion,
			className: "customJSVersion"
		});
	}

	/* versioning display function ends */
	
	/* format versioning code starts, please do not alter or remove these functions */

	function setCreativeVersion()
	{
		EB._sendMessage("SET_CREATIVE_VERSION", {
			creativeVersion: creativeVersion,
			uid: EB._adConfig.uid
		});
		if (typeof displayCreativeVersion === "function")
		{
			displayCreativeVersion();
		}
		setCustomJSVersion();
	}

	function setCustomJSVersion()
	{
		window.addEventListener("message", function(event) {
			try
			{
				var data = JSON.parse(event.data);
				if (!data.data.hasOwnProperty("uid") || data.data.uid !== EB._adConfig.uid)
				{
					return;
				}
				if (data.type === "SET_CUSTOMJS_VERSION")
				{
					if (data.data.hasOwnProperty("customJSVersion"))
					{
						customJSVersion = data.data.customJSVersion;
						if (typeof displayCustomJSVersion === "function")
						{
							displayCustomJSVersion();
						}
					}
				}
			}
			catch (error)
			{
			}
		});	
	}

	/* format versioning code ends, please do not alter or remove these functions */	